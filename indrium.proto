syntax = "proto2";

option optimize_for = LITE_RUNTIME;

package indrium;

enum SensorType
{
    TYPE_TEMPERATURE = 0;
    TYPE_MAGNETICFIELD = 1;
    TYPE_DISTANCE = 2;
    TYPE_BRIGHTNESS = 3;
    TYPE_RELATIVE_HUMIDITY = 4;
    TYPE_ACCELERATION = 5;
    TYPE_ORIENTATION = 6;
    TYPE_GYROSCOPE = 7;
    TYPE_VOLTAGE = 8;
    TYPE_CURRENT = 9;
}

message PapyrChunk
{
    required int32 offset = 1; // offset/index of data in stream
    required bytes data = 2;
}

message Sensor
{
    required SensorType type = 1;
    required int32 instanceID = 2 [default = 1];
    oneof val
    {
        int32 tInt = 3;
        float tFloat = 4;
    }
}

enum IndriumCmdType
{
    CMD_PAPYR_MSG_END = 0;
    CMD_PAPYR_MSG_START_RLE = 1;
    CMD_PAPYR_MSG_START_HEATSHRINK = 2;
    CMD_PAPYR_LAST_OFFSET = 3;
    CMD_BLE_GET_MTU_SIZE = 4;
}

message IndriumCmd
{
    required IndriumCmdType type = 1;
}

message IndriumCmdResponse
{
    oneof type
    {
        int32 intNum = 1;
        float floatNum = 2;
        bytes asBytes = 3;
    }
}

// Tx and RX are from Indrium Device's perspective
// So for example, Papyr device will send a IndriumTx message, while
// the app will send a IndriumRx message

// The base message type is sent by an Indrium device
message IndriumTx
{
    optional int32 timestamp = 1;
    optional bytes DeviceID = 2;
    repeated Sensor sensor = 3;
    optional IndriumCmdResponse resp = 4;
}

// This is sent to the device from the phone for example
message IndriumRx
{
    oneof type
    {
        PapyrChunk chunk = 1;
        IndriumCmd cmd = 2;
    }
}

/*

IndriumCmd usage hint:

The general usage is that the phone/server etc, which sends a IndriumRx mesage
to an indrium device first sends a command type using `IndriumRx.cmd`, then the
indrium device sends a message of type IndriumTx with IndriumCmdResponse(resp)
field present, with the appropriate response. For example, phone sends IndriumRx
with cmd.type=CMD_PAPYR_LAST_OFFSET to ask device which was the last valid
offset it has recieved, the device recieves this and sends a notification with
as IndriumTx packet with resp field with type=intNum to say which was the last
valid offset.

Another example would be CMD_BLE_GET_MTU_SIZE command, which is required because
android/iOS bluetooth APIs do not seem to have a way to find a BLE connection's
current MTU size that. The phone app can send this command to retrive the MTU size
which the firmware does know, and adjust it's own payload size according to the
MTU size.

*/
