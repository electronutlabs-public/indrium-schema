Indrium service protobuf schema
===============================

Why
---

We needed a generic representation / serialization format which
which would let us send and receive multiple kinds of data, like
sensor data or small image data for example, and the assumed transport
is any way you can implement 'message' delivery. Any transport layer
works as long as we have the ability to send a single protobuf
message/frame. MQTT message, HTTP POST payload, ZMQ message etc. work
directly because they already send the payload as a whole, but for
something like BLE, we need a fragmentation/defragmentation method
since a complete 'message' might not fit a single BLE MTU (we have only
20 bytes of data guaranteed, but _can_ be up to 247 bytes for BLE4.2 1M PHY).

Usage
-----

The package name of this schema is `indrium`, and all messages
sent will be of the base types `IndriumTx` if sent by an indrium device, or
`IndriumRx` if sent from an app/server to an indrium device.

### Details of units and representations

`Sensor` message and corresponding units:

    TYPE_TEMPERATURE            Celcius
    TYPE_MAGNETICFIELD          micro Tesla - uT
    TYPE_DISTANCE               cm
    TYPE_BRIGHTNESS             lux
    TYPE_RELATIVE_HUMIDITY      percent
    TYPE_ACCELERATION           m/s^2
    TYPE_ORIENTATION            degrees
    TYPE_GYROSCOPE              rad/s
    TYPE_VOLTAGE                volts
    TYPE_CURRENT                mA

See `indrium.proto` for other types.

BLE transport specification
---------------------------

This is documented here because we are using this first here as a demo
for our [blip](https://github.com/electronut/ElectronutLabs-blip), with
the companion mobile application.

Blip (and any other device supporting Indrium data format) will have the
Indrium service, with the service UUID:
`def9bda9-4370-469c-98d0-7986a8b64034`, and under this service there
will be a characterisitc with the UUID:
`def9bda9-4370-469c-98d0-7986a8b64035`. This characteristic must have
`write` permission, and the `notify` property.

The Indrium serive can be advertised in the advertisement (or scan
response) data too, which makes it easier to filter supported devices
for the mobile application, for example.

Devices that support Indrium BLE service will send notification data only.
The encoded protobuf message might be larger than the current BLE MTU/data
size (20 bytes is guaranteed, up to 247 is possible but it depends on
multiple factors). To send data larger than 19 bytes (OR, the current MTU),
the encoded protobuf may need to be fragmented and sent to the phone, where
it will be reassembled. There is a 2 byte header.

<pre>

Packet looks like -

--------------------------------------
|      prefix     |  data bytes      |
--------------------------------------
|     2 bytes     |     {data}       |
--------------------------------------

For last frame (or first/last in case there is a single chunk), the prefix,
which is also the frame number is set to `0b 0000 0000  0000 0000`. Frame
number starts with `1` otherwise and keeps incrementing.

The frame number is in LSB first format, so frame number 1 will look like `01 00 xx xx xx`, 2nd will look like `02 00 xx xx xx`.

</pre>

Note: for 23 byte MTU, the maximum length for `data` in `IndriumRx.chunk` can be
13 bytes. Check data size after encoding if it is less then max payload. If application
is aware of current MTU size, then it can send larger packets for higher throughput. See
`indrium.proto` for usage hint of `CMD_BLE_GET_MTU_SIZE`.