var protobuf = require("protobufjs");

protobuf.load("../indrium.proto", function(err, root) {
    if (err)
        throw err;
 
    // Obtain a message type
    
    // Exemplary payload
    // var message_type = root.lookupType("indrium.IndriumTx");
    // var type_enum = root.lookupEnum("indrium.SensorType")
    // var payload = {
    //     deviceID : ['a', 'b'],
    //     sensor: 
    //     [
    //         {type : type_enum.values["TYPE_RELATIVE_HUMIDITY"], instanceID: 1, tInt : 52},
    //         {type : type_enum.values["TYPE_VOLTAGE"], instanceID: 2, tFloat : 3.3},
    //     ] 
    // };

    var message_type = root.lookupType("indrium.IndriumRx");
    var payload = {
            chunk : {
                offset : 7000,
                data : [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
            }
    };
 
    // var message_type = root.lookupType("indrium.PapyrChunk");
    // payload = payload.chunk;

    // Verify the payload if necessary (i.e. when possibly incomplete or invalid)
    var errMsg = message_type.verify(payload);
    if (errMsg)
        throw Error(errMsg);

    // Create a new message
    var message = message_type.create(payload); // or use .fromObject if conversion is necessary
    // console.log(message)

    // Encode a message to an Uint8Array (browser) or Buffer (node)
    var buffer = message_type.encode(message).finish();
    // console.log(buffer)
 
    // Decode an Uint8Array (browser) or Buffer (node) to a message
    var message = message_type.decode(buffer);
    // console.log(message)
 
    // If the application uses length-delimited buffers, there is also encodeDelimited and decodeDelimited.
 
    // Maybe convert the message back to a plain object
    var object = message_type.toObject(message, {
        longs: String,
        enums: String,
        bytes: String,
        // see ConversionOptions
    });
    // console.log(object)

    process.stdout.write(buffer)
});